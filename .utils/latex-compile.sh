#!/usr/bin/env bash
set -e

[[ -f "main.tex" ]] || exit 1

# Generate files for bibliography
pdflatex -draftmode -interaction=nonstopmode -halt-on-error main
# Generate bibliography
biber main
# Run twice to output the complete PDF correctly
pdflatex -draftmode -interaction=nonstopmode -halt-on-error main > /dev/null 2>&1 
pdflatex -interaction=nonstopmode -halt-on-error main
