# templatex

The present repository tracks

- a LaTeX template for reports
- a script to build a PDF from it
- CI for GitHub and GitLab

## GitLab output

https://gitlab.com/f3fora/templatex/-/jobs/artifacts/master/file/main.pdf?job=TeX
